import sequtils, strutils, algorithm, strformat, db_sqlite, parsecfg
import strformat

when not defined(release):
  import sugar

import graflib

# TODO
# 1. send package
# 2. look for paths
# 3. set lockers

# TODO
# 1. setup package_db
# 2. add package when sending
# 3. setup paths package

const
  version = "0.1.0"
  info = "Prototype initial release"

proc show(paths: seq[Vertex]) =
  if paths.len == 0:
    echo "No path available"
  else:
    var
      holder = paths.mapIt( it.label ).join( " -> ")
      weight = paths[1..^1].mapIt( it.weight ).foldl(a + b)
    #echo "path: ", holder, " with SLA ", weight, " minute
    echo fmt"path {holder} with SLA {weight}"

proc createFeederRoute(db: var DbConn) =
  db.exec(sql"""
    CREATE TABLE IF NOT EXISTS feeder_route (
      id INTEGER PRIMARY KEY,
      origin VARCHAR(20) NOT NULL,
      destination VARCHAR(20) NOT NULL,
      sla INTEGER,

      -- the value is "inserted" or "inferred"
      type VARCHAR(8) DEFAULT 'inserted',
      paths TEXT,
      created_at TEXT DEFAULT CURRENT_TIMESTAMP,
      updated_at TEXT -- as datetime
    );
  """)

proc createLockersTable(db: var DbConn) =
  db.exec(sql"""
  DROP TABLE IF EXISTS lockers;
  """)
  db.exec(sql"""
CREATE TABLE IF NOT EXISTS lockers (
  label VARCHAR(20) NOT NULL,
  affiliate_ph VARCHAR(20) NOT NULL,
  slot_01 VARCHAR(30),
  slot_02 VARCHAR(30),
  slot_03 VARCHAR(30),
  slot_04 VARCHAR(30),
  slot_05 VARCHAR(30),
  slot_06 VARCHAR(30),
  slot_07 VARCHAR(30),
  slot_08 VARCHAR(30),
  slot_09 VARCHAR(30),
  slot_10 VARCHAR(30),
  slot_11 VARCHAR(30),
  slot_12 VARCHAR(30)
);
  """)

proc insertRoute(db: DbConn, orig, dest: string, sla: int)

proc seedRoutePoint(db: var DbConn) =
  # available PH are
  # menteng, bekasi, malaka, depok, thamrin
  let data = [
    ("menteng", "bekasi", 5),
    ("menteng", "depok", 10),
    ("malaka", "bekasi", 13),
    ("malaka", "depok", 11),
    ("malaka", "thamrin", 7),
    ("thamrin", "menteng", 8),
    ("depok", "bekasi", 10)
  ]
  for d in data:
    db.insertRoute(d[0], d[1], d[2])
    db.insertRoute(d[1], d[0], d[2])

proc seedLockers(db: var DbConn) =
  # need locker for available PH
  let data = [
    ("L01-menteng", "menteng"),
    ("L01-bekasi", "bekasi"),
    ("L01-malaka", "malaka"),
    ("L01-depok", "depok"),
    ("L01-thamrin", "thamrin")
  ]
  db.exec(sql(fmt"""
INSERT INTO lockers (label, affiliate_ph) VALUES
{data.mapIt("(" & it[0] & "," & it[1] & ")").join(",")};
  """))

proc setupDb(conn, dbname: string): DbConn =
  result = open(conn, "", "", dbname)
  var
    config = loadConfig "setup.cfg"
    cleardata = config.getSectionValue("db", "cleardata").parseBool

  if cleardata:
    result.exec(sql"DROP TABLE IF EXISTS feeder_route;")
    result.exec(sql"DROP TABLE IF EXISTS lockers;")

  result.createFeederRoute()
  result.createLockersTable()

  if cleardata:
    config.setSectionKey("db", "cleardata", "no")
    config.writeConfig "setup.cfg"

    result.seedRoutePoint()
    result.seedLockers()

proc retrieveGraph[T, R](db: DbConn): Graph[T,R] =
  result = buildGraph[T, R](weighted = true)

  for row in db.fastRows(sql"select origin,destination,sla from feeder_route;"):
    result.addEdges initEdge[T,R](row[0], row[1], row[2].parseInt)


proc insertRoute(db: DbConn, orig, dest: string, sla: int) =
  db.exec(sql"""
    INSERT INTO feeder_route(origin,destination,sla)
    VALUES(?, ?, ?);""", orig, dest, sla)

proc inferRoute(db: DbConn, graph: var Graph, orig, dest: string): Row =
  let vorig = initVertex(orig, 0)
  let vdest = initVertex(dest, 0)
  if vorig notin graph or vdest notin graph:
    echo "some node not registered yet: $# $#" % [
      if vorig notin graph: orig else: "",
      if vdest notin graph: dest else: ""
    ]
    return @["", "", "", ""]

  var path = graph.shortestPath(vorig, vdest)
  when not defined(release):
    echo "inferred paths: ", path
    show path

  var
    ressla = 0
    resorig = orig
    resdest = dest
    respath = ""
  template insertFeederRoute(org, dst: string, sla: int, path: string): untyped =
    db.exec(sql"""
      INSERT INTO feeder_route(origin, destination, sla, paths, type)
      VALUES(?,?,?,?,'inferred');""",
      org, dst, sla, path)

  if path.len >= 2:
    let
      orignode = path[0]
      destnode = path[^1]
      pathtraverse = path[1..^2].mapIt( it.label ).join("-")
      sla = path[1..^1].mapIt( it.weight ).foldl(a + b)
      #pathreverse = pathtraverse.split("-").reversed.join("-")
      pathreverse = path[1..^2].mapIt( it.label ).reversed.join("-")
    #if idx == 0:
    ressla = sla
    resorig = orignode.label
    resdest = destnode.label
    respath = pathtraverse

    insertFeederRoute(orignode.label, destnode.label, sla, pathtraverse)
    insertFeederRoute(destnode.label, orignode.label, sla, pathreverse)

  @[resorig, resdest, $ressla, respath]

proc retrieveRoute(db: DbConn, graph: var Graph, orig, dest: string): Row =
  try:
    result = db.getRow(sql"""
      SELECT origin,destination,sla,paths FROM feeder_route
      WHERE origin=? and destination=?
      ORDER BY sla ASC;""", orig, dest)
    if result.anyIt( it == ""):
      db.dbError
  except DbError:
    result = db.inferRoute(graph, orig, dest)

proc displayInfo(orig, dest: string, sla: int, paths: string) =
  echo "\t===================="
  echo "\torigin: ", orig
  echo "\tdestination: ", dest
  echo "\tsla: ", sla
  echo "\tpaths: ", paths
  echo "\t===================="

proc listAll(db: DbConn, maxrow = -1) =
  var count = 0
  for row in db.fastRows(sql"select origin,destination,sla,paths from feeder_route;"):
    if maxrow != -1:
      inc count
    displayInfo(row[0], row[1], row[2].parseInt, row[3])
    if count == maxrow:
      break

proc nodeList(g: Graph, qline = -1) =
  var total = 0
  echo "\t========="
  for i, v in g.vertices:
    echo "\t", i+1, ": ", v.label
    inc total
    if total == qline:
      break

  if qline != -1:
    total = g.vertices.len

  echo "\ttotal nodes: ", total
  echo "\t========="

proc help =
  echo fmt"""
package_route
version: {version}
release_info: {info}

command:
  add origin destination sla
  route origin destination
  list [querynum]
  node [querynum]
  total
  help
  quit
"""

proc command(db: DbConn, graph: var Graph) =
  var running = true
  while running:
    stdout.write("> ")
    var
      command = stdin.readLine
      cmds = command.split(" ")
    case cmds[0]
    of "add":
      if cmds[1..^1].len < 3:
        echo """Please insert in this format "add maluku bandar 50""""
      else:
        let sla = try: cmds[3].parseInt
                  except ValueError: -1
        db.insertRoute(cmds[1], cmds[2], sla)
        db.insertRoute(cmds[2], cmds[1], sla)
        graph.addEdges initEdge(cmds[1], cmds[2], sla)
        echo "Route added"

    of "route":
      if cmds[1..^1].len < 2:
        echo """Please insert in this format "route maluku bandar""""
      else:
        var row = db.retrieveRoute(graph, cmds[1], cmds[2])
        when not defined(release):
          echo command
          echo row
        try:
          displayInfo(row[0], row[1], row[2].parseInt, row[3])
        except:
          echo "Some error happened"

    of "list":
      var queryline = -1
      if cmds.len == 2:
        try:
          queryline = cmds[1].parseInt
        except ValueError:
          echo "Please insert a correct number for listing"
          return
      db.listAll queryline

    of "node":
      var qline = -1
      if cmds.len == 2:
        try: qline = cmds[1].parseInt
        except ValueError:
          echo "Incorrect number for listing"
          return
      graph.nodeList qline

    of "quit":
      running = false
      echo "goodbye"

    of "total":
      echo fmt"""Available routes is {db.getRow(sql"select count(*) from feeder_route;")[0]}"""
      echo fmt"""from {graph.vertices.len} nodes"""

    of "send":
      discard

    of "":
      help()

    else:
      help()


proc main =
  var db = setupDb("routes.db", "feeder_route")
  var graph = retrieveGraph[string, int](db)

  db.command graph


main()
